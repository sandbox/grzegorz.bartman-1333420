<?php
/**
 * @file
 * Administration forms.
 */

/**
 * Module settings form callback.
 */
function context_htmltags_settings() {
  $form = array();
  $form['context_htmltags_tags_ids'] = array(
      '#type' => 'textarea',
      '#title' => t('Identificators of HTML tags'),
      '#description' => t(
              'Type tag indentyficator per line. Each tag id can be used in tpl files.'),
      '#default_value' => variable_get('context_htmltags_tags_ids', ''),
  );
  return system_settings_form($form);
}
