<?php
/**
 * @file
 * Context plugin definition.
 */

/**
 * Allow additional CSS classes to be defined for blocks per context.
 */
class context_htmltags_reaction_tags extends context_reaction {

  /**
   * Options form.
   */
  function options_form($context) {
    $values = $this->fetch_from_context($context);
    $form = array();

    $tags_ids = variable_get('context_htmltags_tags_ids', '');

    if (isset($tags_ids)) {
      $tags_ids_array = explode("\n", $tags_ids);
      foreach ($tags_ids_array as $tag_id) {
        $tag_id = trim($tag_id);
        $form[$tag_id] = array(
            '#type' => 'fieldset',
            '#title' => $tag_id,
        );
        $form[$tag_id]['tag_html'] = array(
            '#title' => t('Type HTML tag for "' . $tag_id . '"'),
            '#type' => 'textfield',
            '#description' => t('Type HTML tag. Example if you want use [h1]sample text[/h1] type in form just h1'),
            '#default_value' => $values[$tag_id]['tag_html'],
        );
        $form[$tag_id]['tag_class'] = array(
            '#title' => t('Class'),
            '#type' => 'textfield',
            '#description' => t('Optional field. Type if you want add some css class'),
            '#default_value' => $values[$tag_id]['tag_class'],
        );
      }
    }
    else {
      $form['tag_empty'] = array(
          '#value' => l('Go to configuration and add some tag ids', 'admin/settings/context_htmltags'),
      );
    }
    return $form;
  }

}
