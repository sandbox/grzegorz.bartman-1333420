Context HTML Tags module - use this if you want change some html tags depends on context.
The module can be useful for SEO optimization.

==Instructions==
1.Go to admin/settings/context_htmltags and define some tags identificators (one per line), 
for example type in form: page_title

2. Put in file function context_htmltags_tag($title, 'page_title', 'h1', 'title'), example:
Insert the followind code into a page.tpl.php file instead of a <?php print $title; ?>:
<?php print context_htmltags_tag($title, 'page_title', 'h1', 'title'); ?>

3. If You want change this HTML tag in some context go to admin/build/context and add reaction "HTML tag". 
Set HTML tags and css class to selected tags identificators.



==Author==
Grzegorz Bartman
grzegorz.bartman@openbit.pl
www.openbit.pl
